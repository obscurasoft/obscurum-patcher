package org.obscurasoft.opatch.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

import org.obscurasoft.opatch.Main;
import org.obscurasoft.opatch.gui.Progress;
import org.obscurasoft.opatch.thread.Patch;

public class Listener implements ActionListener {
	public static Progress progress = new Progress("Patching");
	public static Patch patch = new Patch();

	public Listener() {

	}

	public void actionPerformed(ActionEvent event) {
		if (event.getActionCommand() == "Выход") {
			System.exit(0);
		} else if (event.getActionCommand() == "Обновить") {
			Main.frame.setVisible(false);
			progress.setVisible(true);
			new Thread(patch).start();
			progress.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		} else if (event.getActionCommand() == "О программе") {

		}
	}
}
