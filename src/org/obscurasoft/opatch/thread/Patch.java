package org.obscurasoft.opatch.thread;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.obscurasoft.opatch.gui.OopsFrame;
import org.obscurasoft.opatch.gui.Progress;
import org.obscurasoft.opatch.gui.SucFrame;
import org.obscurasoft.opatch.listener.Listener;

/**
 * 
 * @author GodusX Patching thread. Checks files,
 *
 */

public class Patch extends Thread {
	public static String listFiles[] = { "assets" };
	public static ActionListener listener = new Listener();
	public static OopsFrame err = new OopsFrame("Ошибка!");
	public static SucFrame suc = new SucFrame("Успех!");

	public Patch() {

	}

	public static boolean delete(String s) {
		boolean bool;
		File tmpvar = new File(s);
		if (tmpvar.exists()) {
			bool = tmpvar.delete();
			if (bool) {
				return true;
			}
		}
		return false;
	}

	public static boolean exists(String s) {
		File tmpvar = new File(s);
		if (tmpvar.exists()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean check() {
		boolean exist;
		int sum = 0;
		Progress.label.setText("Checking files...");
		for (int i = 0; i != listFiles.length; i++) {
			exist = exists(listFiles[i]);
			Progress.file.setText(listFiles[i]);
			if (exist == true) {
				sum = sum + 1;
			}
		}
		Progress.file.setText("");
		if (sum == listFiles.length) {
			return true;
		} else {
			return false;
		}
	}

	public void patch() {
		for (int i = 0; i != listFiles.length; i++) {
			Progress.file.setText(listFiles[i]);
			try {
				Files.copy(Paths.get(listFiles[i]), Paths.get("%appdata%\\.tlauncher\\"),
						(CopyOption) StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException ioe) {
				cancel();
			}
		}
	}

	public void cleanup() {
		for (int i = 0; i != listFiles.length; i++) {
			Progress.file.setText(listFiles[i]);
			delete(listFiles[i]);
		}
	}

	public void cancel() {
		Listener.progress.setFocusable(false);
		err.setVisible(true);
		this.interrupt();
	}

	public void run() {
		boolean success = false;
		success = check();
		if (success) {
			patch();
			cleanup();
		} else {
			cancel();
		}
	}
}
