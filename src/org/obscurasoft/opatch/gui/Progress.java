package org.obscurasoft.opatch.gui;

import java.awt.Color;
import java.awt.Label;
import javax.swing.JFrame;

public class Progress extends JFrame {

	/**
	 * Patching monitor
	 */
	private static final long serialVersionUID = 1L;
	ProgressPanel panel = new ProgressPanel();
	ProgBar progbar = new ProgBar();
	public static Label label = new Label("State");
	public static Label file = new Label("File");

	public Progress(String title) {
		super(title);
		this.setBackground(Color.GRAY);
		file.setText("NULL");
		this.setLocationByPlatform(true);
		this.setSize(500, 100);
		label.setBounds(15, 15, 100, 15);
		file.setBounds(115, 15, 475, 15);
		this.setResizable(false);
		progbar.setBounds(50, 40, 400, 20);
		this.add(label);
		this.add(file);
		this.add(progbar);
		this.add(panel);
	}

}
