package org.obscurasoft.opatch.gui;

import java.awt.Button;
import java.awt.Label;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.obscurasoft.opatch.listener.Listener;

public class SucPanel extends JPanel {

	/**
	 * Success panel
	 */
	private static final long serialVersionUID = 1L;
	Label label = new Label();
	Button exit = new Button("Выход");
	Label skip = new Label();
	ActionListener listener = new Listener();

	public SucPanel() {
		skip.setText("        ");
		label.setText("   Клиент обновлён! Теперь вы можете играть.");
		this.add(label);
		this.add(skip);
		exit.addActionListener(listener);
		this.add(exit);
	}
}
