package org.obscurasoft.opatch.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class OopsFrame extends JFrame {

	/**
	 * Error window
	 */
	private static final long serialVersionUID = 1L;
	public static OopsPanel panel = new OopsPanel();

	public OopsFrame(String title) {
		super(title);
		this.setResizable(false);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setLayout(new BorderLayout(0, 1));
		this.setSize(350, 100);
		this.add(panel);
	}

}
