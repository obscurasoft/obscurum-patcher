package org.obscurasoft.opatch.gui;

import java.awt.BorderLayout;
import javax.swing.JFrame;

public class MainFrame extends JFrame {
	/**
	 * Creating main window
	 */
	private static final long serialVersionUID = 1L;
	MainPanel panel = new MainPanel();

	public MainFrame(String title) {
		super(title);
		this.setLayout(new BorderLayout(0, 1));
		this.setSize(500, 100);
		this.add(panel);
	}

}
