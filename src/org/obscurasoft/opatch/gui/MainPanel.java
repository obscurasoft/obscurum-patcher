package org.obscurasoft.opatch.gui;

import java.awt.Button;
import java.awt.Label;
import java.awt.event.ActionListener;
import javax.swing.JPanel;

import org.obscurasoft.opatch.Main;
import org.obscurasoft.opatch.listener.Listener;

public class MainPanel extends JPanel {
	/**
	 * Main panel for elements
	 */
	private static final long serialVersionUID = 1L;

	Label label = new Label();
	Button patch = new Button("Обновить");
	Button exit = new Button("Выход");
	Button about = new Button("О программе");
	Label skip = new Label();
	Label skip2 = new Label();
	ActionListener listener = new Listener();

	public MainPanel() {
		super();
		label.setText(Main.version + " для " + Main.mine + " обновит ваш клиент до " + Main.patchTo);
		skip.setText("              ");
		skip2.setText("              ");
		patch.addActionListener(listener);
		about.addActionListener(listener);
		exit.addActionListener(listener);
		this.add(label);
		this.add(patch);
		this.add(skip);
		// TODO: Make "About" window
		// this.add(about);
		this.add(skip2);
		this.add(exit);
	}
}
