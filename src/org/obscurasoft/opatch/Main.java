package org.obscurasoft.opatch;

import javax.swing.JFrame;
import net.godusx.opatch.gui.MainFrame;

/**
 * 
 * @author ObscuraSoft group
 * Main patcher class
 *
 */

public class Main {
	public static final String version = "Patcher v0.2";
	public static final String mine = "ObscurumCraft E25";
	public static final String patchTo = "ObscurumCraft B25";
	public static MainFrame frame = new MainFrame(Main.version);
	
	public static void createGui() {
		//Let's make some default settings
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setAlwaysOnTop(true);
		frame.setResizable(false);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
	}
	
	public static void main(String args[]) {
		createGui();
	}
}
